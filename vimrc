syntax on
set autoindent
set number
set tabstop=4 shiftwidth=4 expandtab
" Ignore cases in finder
set ignorecase

" Colour scheme
set t_Co=256
highlight Comment cterm=bold
highlight LineNr ctermfg=grey

" Use system clipboard (you need vim-clipboard/vim-x11 or gvim)
" Documentation --> https://vim.fandom.com/wiki/Accessing_the_system_clipboard
set clipboard=unnamed
map <MiddleMouse> p

" Indent with the pipe symbol
let g:indentLine_char = '│'

" NerdTREE mappings
let NERDTreeMinimalUI = 1
map <F2> :NERDTreeToggle<CR>
" Open vertical split
let NERDTreeMapOpenVSplit = 'i'
" Open horizontal split
let NERDTreeMapOpenSplit = 'h'

" Use tab to explore buffers
map <tab> <C-w>

" Remap vim-latex-live-preview
map <F6> :LLPStartPreview<CR>
" Remap vim-flake8
autocmd FileType python map <buffer> <F6> :call flake8#Flake8()<CR>

" Go to tab by number (used in conjunction with tabulous)
" Documentation --> https://vim.fandom.com/wiki/Alternative_tab_navigation
" M == ALT (works with NON VTE-based terminals such as xterm (you need 8 bits))
map <M-1> :NERDTreeClose<CR> <bar> 1gt
map <M-2> :NERDTreeClose<CR> <bar> 2gt
map <M-3> :NERDTreeClose<CR> <bar> 3gt
map <M-4> :NERDTreeClose<CR> <bar> 4gt
map <M-5> :NERDTreeClose<CR> <bar> 5gt
map <M-6> :NERDTreeClose<CR> <bar> 6gt
map <M-7> :NERDTreeClose<CR> <bar> 7gt
map <M-8> :NERDTreeClose<CR> <bar> 8gt
map <M-9> :NERDTreeClose<CR> <bar> 9gt
map <M-0> :NERDTreeClose<CR> <bar> :tabnext<CR>

" Turn on pathogen
function! Pathogen_on()
  execute pathogen#infect()
  call pathogen#helptags()
endfunction

" Use language specific settings
filetype plugin on
"then in your .vim/ftplugin directory
"make files like c.vim and cpp.vim etc these 
"will be loaded when a specific file is edited. 
"The c.vim file is "the same syntax as vimrc

" Call functions
call Pathogen_on()

