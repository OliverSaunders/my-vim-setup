vim_setup_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

rm -rf ~/.vim
rm -rf ~/.vimpkg

# 𝘂𝗽𝗱𝗮𝘁𝗲 𝘃𝗶𝗺𝗿𝗰 & .𝘃𝗶𝗺
echo "Copying custom vimrc and .vim directories"
cd $vim_setup_dir

# add current_browser and replace .desktop with nothing
current_browser="$(xdg-settings get default-web-browser)"
current_browser="${current_browser//.desktop/}"
# replace browser with current
sed -i 's/firefox/'$current_browser'/g' .vim/ftplugin/html.vim

cat vimrc >> ~/.vimrc
\cp -r .vim ~/.vim

# 𝗜𝗻𝘀𝘁𝗮𝗹𝗹 𝘃𝗶𝗺 𝗽𝗮𝗰𝗸𝗮𝗴𝗲𝘀
echo "Installing vim packages"
cd ~/

# Install pathogen
echo "Installing pathogen"
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

# Install apt-vim
echo "Installing apt-vim"
curl -sL https://raw.githubusercontent.com/egalpin/apt-vim/master/install.sh | sh
source ~/.bashrc

# Install NERDTree
echo "Installing NERDTree"
apt-vim install -y https://github.com/preservim/nerdtree.git

# Install vim-indent-guides
echo "Installing indentLine"
apt-vim install -y https://github.com/Yggdroot/indentLine.git

# Install tabulous
echo "Installing tabulous"
apt-vim install -y https://github.com/webdevel/tabulous.git

# Install auto-pairs-gentle
echo "Installing auto-pairs-gentle"
apt-vim install -y https://github.com/vim-scripts/auto-pairs-gentle.git

# Install vim-alt-mappings
echo "Installing vim-alt-mappings"
apt-vim install -y apt-vim install https://github.com/vim-utils/vim-alt-mappings.git

# Install vim-css-color
echo "Installing vim-css-color"
apt-vim install -y https://github.com/ap/vim-css-color.git

# Install vim-flake8
echo "Installing vim-flake8"
apt-vim install -y https://github.com/nvie/vim-flake8.git
pip install flake8

# Install vim-latex-live-preview
echo "Installing vim-latex-live-preview"
apt-vim install -y https://github.com/xuhdev/vim-latex-live-preview.git

# If you get vim errors
echo "if you get 'Can't open file /usr/share/vim/syntax/syntax.vim' when opening vim please reinstall vim-runtime"
echo "if you get a pip error please install python or remove vim-flake8 from the autoscript"
